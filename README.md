![Banner](./banner.jpg)

# Taller de Git 2022

Recursos del Taller "Aprende Git como si estuvieras en primero", impartido para el #hacktoberfest 2022, impartido el 6 de Octubre de 2022 por (Orden de Izquierda a Derecha):

<img src="./photos/photo_2022-10-06_19-37-33.jpg" alt="Foto de los creadores del taller" width="200"/>

- [David Lopex Geraghty](https://lopezgeraghty.com)
- [Alejandro Barrachina Argudo](https://github.com/ALK222)
- [David Davó Laviña](https://ddavo.me/es)

## Diapositivas
- [Aprende Git Como si estuvieras en primero](./slides/2022-10-06.pdf)

## Licencia

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
